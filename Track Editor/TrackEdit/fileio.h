#ifndef _FILELOAD_H
#define _FILELOAD_H

//Function prototypes exported from fileload.cpp

#include "UnitInfo.h"

using namespace std;

bool ReadRTUFile(const string& pathname, TRACKTHEME* theme);
bool NamedFileExists(const char* filename);
bool GetFileDescription(const char* filename, char* description);
bool DescribedFileExists(const char* description);
bool GetDescribedFilename(const char* description, char* filename);
bool NextFreeFile(char* filename);
bool NextFreeDescription(char* description);
bool NamedLevelExists(const char* levelname);
bool GetLevelDescription(const char* levelname, char* description);
bool GetDescribedLevelName(const char* description, char* levelname);
bool DescribedLevelExists(const char* description);
bool NextFreeLevel(char* levelname);
bool WriteTDF(const char* filename, TRACKDESC* track);
bool ReadTDF(const char* filename, TRACKDESC* track, bool convert);
bool MakeFileList(void);
void EraseFileList(void);
void WriteWorldFile(const char *filename);
void WriteInfFile(const char *filename, const char* description, const RevoltVertex& startpos, REAL startdir);
void WriteLITFile(const char *filename, const RevoltVertex& startpos);
void WriteNCPFile(const char* destfile);
void WriteTAZFile(const char* destfile);
void WritePANFile(const char* destfile);
void WriteFOBFile(const char* destfile, const TRACKDESC* track, const TRACKTHEME* theme);
void WriteFANFile(const char* destfile);
void CopyBitmaps(const char* levelroot);
void DeleteDirContents(const char* dirname);
void DeleteDirectory(const char* dirname);
void DeleteTrackFiles(INDEX index);

//const U16 RTU_READ_VERSION = 1;	//now includes UV information
//const U16 RTU_READ_VERSION = 2;	//now includes surface material information
//const U16 RTU_READ_VERSION = 3;	//now includes track zone information
//const U16 RTU_READ_VERSION = 4;	//no longer includes VALID_EXITS info
//const U16 RTU_READ_VERSION = 5;	//now includes a pickup position for each unit
//const U16 RTU_READ_VERSION = 6;	//now includes a AI Node info for each module
//const U16 RTU_READ_VERSION = 7;	//version 7 - now includes wall geometry
//const U16 RTU_READ_VERSION = 8;	//version 8 - now includes gouraud information
//const U16 RTU_READ_VERSION = 9; 	//version 9 - now includes root edge requirement data for each unit
//const U16 RTU_READ_VERSION = 10; 	//version 10 - now optimised (uses U16 instead of U32, etc)
//const U16 RTU_READ_VERSION = 11; 	//version 11 - now pools the polysets and also packs UVPolyInstance data
//const U16 RTU_READ_VERSION = 12; 	//version 12 - now reads light information for modules
const U16 RTU_READ_VERSION = 13; 	//version 13 - Fixed alignment problems

enum
{
	TDF_READ_VERSION_1 = 1,
	TDF_READ_VERSION_2,
};

//const U16 TDF_READ_VERSION = TDF_READ_VERSION_1;		
const U16 TDF_READ_VERSION = TDF_READ_VERSION_2;		//now includes pickup info

#endif //_FILELOAD_H