#ifndef __UNITINFO_H
#define __UNITINFO_H

#include <string>
#include "modules.h"

typedef struct
{
	TRACKMODULE**		Modules;
	TRACKUNIT**			Units;
	MESH*				Meshes;
	BASICPOLY*			Polys;
	BASICPOLY*			RGBPolys;
	BASICPOLY*			UVPolys;
	REVOLTVECTOR*		Verts;
	RevoltUVCoord*		UVCoords;
	POLYSET*			PolySets;
	MODULE_LOOKUP_INFO*	Lookup;
	U32					UnitCount;
	U32					UVCoordCount;
	U32					UVPolyCount;
	U32					VertCount;
	U32					PolyCount;
	U32					RGBPolyCount;
	U32					MeshCount;
	U32					ModuleCount;
	U32					TPageCount;
	U32					PolySetCount;
	U32					WallIndex;
}TRACKTHEME;

enum {PEG_INDEX, PAN_INDEX, HULL_INDEX};
enum {MODULE_SPACER, MODULE_STARTGRID, MODULE_REAL_SPACER};
enum {UNIT_SPACER, UNIT_STARTGRID, UNIT_REAL_SPACER};

#endif //__UNITINFO_H