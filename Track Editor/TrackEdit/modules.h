#ifndef _MODULES_H
#define _MODULES_H

#include <bobtypes.h>
#include "TrackEditTypes.h"

enum {
	TWM_SPACE, //space_unit
	TWM_START, //start_grid
	TWM_SPACE_B, //space_unit
	TWM_BANK_05, //b_05
	TWM_BANK_10, //b_10
	TWM_BANK_20, //b_20
	TWM_BANK_05_2, //b_05_2
	TWM_BANK_10_2, //b_10_2
	TWM_BANK_20_2, //b_20_2
	TWM_BANK_05_2_LH, //b_05_2_LH
	TWM_BANK_10_2_LH, //b_10_2_LH
	TWM_BANK_20_2_LH, //b_20_2_LH
	TWM_BANK_05_2_N, //b_05_2_N
	TWM_BANK_10_2_N, //b_10_2_N
	TWM_BANK_20_2_N, //b_20_2_N
	TWM_BANK_CORNER_05, //bc_05
	TWM_BANK_CORNER_10, //bc_10
	TWM_BANK_CORNER_20, //bc_20
	TWM_BANK_CORNER_05_2, //bc_05_2
	TWM_BANK_CORNER_10_2, //bc_10_2
	TWM_BANK_CORNER_20_2, //bc_20_2
	TWM_BANK_IN_05, //bino_05
	TWM_BANK_IN_10, //bino_10
	TWM_BANK_IN_20, //bino_20
	TWM_BANK_IN_05_2, //bino_05_2
	TWM_BANK_IN_10_2, //bino_10_2
	TWM_BANK_IN_20_2, //bino_20_2
	TWM_BANK_IN_05_LH, //bino_05_LH
	TWM_BANK_IN_10_LH, //bino_10_LH
	TWM_BANK_IN_20_LH, //bino_20_LH
	TWM_BANK_IN_05_2_LH, //bino_05_2_LH
	TWM_BANK_IN_10_2_LH, //bino_10_2_LH
	TWM_BANK_IN_20_2_LH, //bino_20_2_LH
	TWM_BANK_IN_05_2_D, //bino_05_2_D
	TWM_BANK_IN_10_2_D, //bino_10_2_D
	TWM_BANK_IN_20_2_D, //bino_20_2_D
	TWM_BANK_IN_05_2_LH_D, //bino_05_2_LH_D
	TWM_BANK_IN_10_2_LH_D, //bino_10_2_LH_D
	TWM_BANK_IN_20_2_LH_D, //bino_20_2_LH_D
	TWM_BANK_IN_05_2_N, //bino_05_2_N
	TWM_BANK_IN_10_2_N, //bino_10_2_N
	TWM_BANK_IN_20_2_N, //bino_20_2_N
	TWM_BANK_IN_05_2_LH_N, //bino_05_2_LH_N
	TWM_BANK_IN_10_2_LH_N, //bino_10_2_LH_N
	TWM_BANK_IN_20_2_LH_N, //bino_20_2_LH_N
	TWM_BRIDGE_10, //bridge_10
	TWM_BRIDGE_15, //bridge_15
	TWM_BRIDGE_20, //bridge_20
	TWM_BRIDGE_25, //bridge_25
	TWM_BRIDGE_30, //bridge_30
	TWM_BRIDGE_35, //bridge_35
	TWM_BRIDGE_40, //bridge_40
	TWM_BRIDGE_45, //bridge_45
	TWM_BRIDGE_50, //bridge_50
	TWM_BRIDGE_55, //bridge_55
	TWM_BRIDGE_60, //bridge_60
	TWM_BRIDGE_65, //bridge_65
	TWM_BRIDGE_70, //bridge_70
	TWM_BRIDGE_75, //bridge_75
	TWM_BRIDGE_80, //bridge_80
	TWM_BRIDGE_10_2, //bridge_10_2
	TWM_BRIDGE_15_2, //bridge_15_2
	TWM_BRIDGE_20_2, //bridge_20_2
	TWM_BRIDGE_25_2, //bridge_25_2
	TWM_BRIDGE_30_2, //bridge_30_2
	TWM_BRIDGE_35_2, //bridge_35_2
	TWM_BRIDGE_40_2, //bridge_40_2
	TWM_BRIDGE_45_2, //bridge_45_2
	TWM_BRIDGE_50_2, //bridge_50_2
	TWM_BRIDGE_55_2, //bridge_55_2
	TWM_BRIDGE_60_2, //bridge_60_2
	TWM_BRIDGE_65_2, //bridge_65_2
	TWM_BRIDGE_70_2, //bridge_70_2
	TWM_BRIDGE_75_2, //bridge_75_2
	TWM_BRIDGE_80_2, //bridge_80_2
	TWM_BRIDGE_10_2_LH, //bridge_10_2_LH
	TWM_BRIDGE_15_2_LH, //bridge_15_2_LH
	TWM_BRIDGE_20_2_LH, //bridge_20_2_LH
	TWM_BRIDGE_25_2_LH, //bridge_25_2_LH
	TWM_BRIDGE_30_2_LH, //bridge_30_2_LH
	TWM_BRIDGE_35_2_LH, //bridge_35_2_LH
	TWM_BRIDGE_40_2_LH, //bridge_40_2_LH
	TWM_BRIDGE_45_2_LH, //bridge_45_2_LH
	TWM_BRIDGE_50_2_LH, //bridge_50_2_LH
	TWM_BRIDGE_55_2_LH, //bridge_55_2_LH
	TWM_BRIDGE_60_2_LH, //bridge_60_2_LH
	TWM_BRIDGE_65_2_LH, //bridge_65_2_LH
	TWM_BRIDGE_70_2_LH, //bridge_70_2_LH
	TWM_BRIDGE_75_2_LH, //bridge_75_2_LH
	TWM_BRIDGE_80_2_LH, //bridge_80_2_LH
	TWM_BRIDGE_10_2_N, //bridge_10_2_N
	TWM_BRIDGE_15_2_N, //bridge_15_2_N
	TWM_BRIDGE_20_2_N, //bridge_20_2_N
	TWM_BRIDGE_25_2_N, //bridge_25_2_N
	TWM_BRIDGE_30_2_N, //bridge_30_2_N
	TWM_BRIDGE_35_2_N, //bridge_35_2_N
	TWM_BRIDGE_40_2_N, //bridge_40_2_N
	TWM_BRIDGE_45_2_N, //bridge_45_2_N
	TWM_BRIDGE_50_2_N, //bridge_50_2_N
	TWM_BRIDGE_55_2_N, //bridge_55_2_N
	TWM_BRIDGE_60_2_N, //bridge_60_2_N
	TWM_BRIDGE_65_2_N, //bridge_65_2_N
	TWM_BRIDGE_70_2_N, //bridge_70_2_N
	TWM_BRIDGE_75_2_N, //bridge_75_2_N
	TWM_BRIDGE_80_2_N, //bridge_80_2_N
	TWM_CORNER_SQUARE, //c_basic
	TWM_CORNER_SQUARE_2, //c_basic_2
	TWM_CORNER_REGULAR, //c_l_r
	TWM_CORNER_REGULAR_2, //c_l_r_2
	TWM_CORNER_REGULAR_LH, //c_l_r_LH
	TWM_CORNER_REGULAR_2_LH, //c_l_r_2_LH
	TWM_CORNER_REGULAR_N, //c_l_r_N
	TWM_CORNER_REGULAR_2_N, //c_l_r_2_N
	TWM_CORNER_MEDIUM, //c_l_x
	TWM_CORNER_MEDIUM_2, //c_l_x_2
	TWM_CORNER_MEDIUM_LH, //c_l_x_LH
	TWM_CORNER_MEDIUM_2_LH, //c_l_x_2_LH
	TWM_CORNER_MEDIUM_N, //c_l_x_N
	TWM_CORNER_MEDIUM_2_N, //c_l_x_2_N
	TWM_CORNER_LARGE, //c_l_xt
	TWM_CORNER_LARGE_2, //c_l_xt_2
	TWM_CORNER_LARGE_LH, //c_l_xt_LH
	TWM_CORNER_LARGE_2_LH, //c_l_xt_2_LH
	TWM_CORNER_LARGE_N, //c_l_xt_N
	TWM_CORNER_LARGE_2_N, //c_l_xt_2_N
	TWM_DIP_R, //d_r
	TWM_DIP_X, //d_x
	TWM_DIP_XT, //d_xt
	TWM_DIP_R_2, //d_r_2
	TWM_DIP_X_2, //d_x_2
	TWM_DIP_XT_2, //d_xt_2
	TWM_HUMP_R, //h_r
	TWM_HUMP_X, //h_x
	TWM_HUMP_XT, //h_xt
	TWM_HUMP_R_2, //h_r_2
	TWM_HUMP_X_2, //h_x_2
	TWM_HUMP_XT_2, //h_xt_2
	TWM_NARROW, //narrow
	TWM_NARROWF, //narrowf
	TWM_NARROWF_2, //narrowf_2
	TWM_PIPE_0, //p_(0)
	TWM_PIPE_1, //p_(1)
	TWM_PIPE_2, //p_(2)
	TWM_PIPE_20_0, //p_20_(0)
	TWM_PIPE_20_1_BOT, //p_20_(1,bot)
	TWM_PIPE_20_1_TOP, //p_20_(1,top)
	TWM_PIPE_20_2, //p_20_(2)
	TWM_PIPEC_0, //pc_(0)
	TWM_PIPEC_1, //pc_(1)
	TWM_PIPEC_1A, //pc_(1a)
	TWM_PIPEC_2, //pc_(2)
	TWM_PIPEF, //pf
	TWM_PIPE_1A, //p_(1a)
	TWM_PIPEF_OUT, //pf_out
	TWM_PIPEF_N, //pf_n
	TWM_RAMP_00, //r_00
	TWM_RAMP_05, //r_05
	TWM_RAMP_10, //r_10
	TWM_RAMP_15, //r_15
	TWM_RAMP_20, //r_20
	TWM_RAMP_25, //r_25
	TWM_RAMP_30, //r_30
	TWM_RAMP_00_2, //r_00_2
	TWM_RAMP_05_2, //r_05_2
	TWM_RAMP_10_2, //r_10_2
	TWM_RAMP_15_2, //r_15_2
	TWM_RAMP_20_2, //r_20_2
	TWM_RAMP_25_2, //r_25_2
	TWM_RAMP_30_2, //r_30_2
	TWM_RAMP_05_2_D, //r_05_2_D
	TWM_RAMP_10_2_D, //r_10_2_D
	TWM_RAMP_15_2_D, //r_15_2_D
	TWM_RAMP_20_2_D, //r_20_2_D
	TWM_RAMP_25_2_D, //r_25_2_D
	TWM_RAMP_30_2_D, //r_30_2_D
	TWM_RAMP_05_2_N, //r_05_2_N
	TWM_RAMP_10_2_N, //r_10_2_N
	TWM_RAMP_15_2_N, //r_15_2_N
	TWM_RAMP_20_2_N, //r_20_2_N
	TWM_RAMP_25_2_N, //r_25_2_N
	TWM_RAMP_30_2_N, //r_30_2_N
	TWM_DIAGONAL_01, //sf_01
	TWM_DIAGONAL_02, //sf_02
	TWM_DIAGONAL_01_2, //sf_01_2
	TWM_DIAGONAL_02_2, //sf_02_2
	TWM_DIAGONAL_01_LH, //sf_01_LH
	TWM_DIAGONAL_02_LH, //sf_02_LH
	TWM_DIAGONAL_01_2_LH, //sf_01_2_LH
	TWM_DIAGONAL_02_2_LH, //sf_02_2_LH
	TWM_RUMBLE_02, //st_02
	TWM_RUMBLE_04, //st_04
	TWM_RUMBLE_02_2, //st_02_2
	TWM_RUMBLE_04_2, //st_04_2
	TWM_CROSS_ROAD, //X
	TWM_ZIGZAG, //zigzag
	TWM_ZIGZAG_2, //zigzag_2
	TWM_JUMP,	  //jump unit
	TWM_RAMP_00_2_N, //r_00_2_n
};

typedef struct
{
	U16	Original;		//the ID for the module which corresponds to this entry
	U16 PreviousVar;	//the ID to change to in response to the 'previous variant' command
	U16 NextVar;		//the ID to change to in response to the 'next variant' command
	U16 Forward;		//the ID to change to to make this module face in the forward direction
	U16 Reverse;		//the ID to change to to make this module face in the reverse direction
}MODULE_CHANGES;

#if !defined ELEMENTS_IN
  #define ELEMENTS_IN(aa) (sizeof(aa)/sizeof((aa)[0]))
#endif

typedef struct
{
	INDEX	ModuleID;
	INDEX	AlternateID;
	INDEX	TextIndex;
}MODULE_GROUP_INFO;

typedef struct
{
	MODULE_GROUP_INFO*	Groups;
	U16					GroupCount;
	MODULE_CHANGES*		Changes;
}MODULE_LOOKUP_INFO;

#endif