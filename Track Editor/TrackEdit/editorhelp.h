#ifndef _EDITORHELP_H
#define _EDITORHELP_H

#include <typedefs.h>
#include <bobtypes.h>

typedef struct
{
	INDEX	KeyText;
	INDEX	ActionText;
	REAL	ActionXPos;
	REAL	DialogXPos;
	REAL	DialogYPos;
	U16		DialogWidth;
	U16		DialogHeight;
}HELP_DIALOG_INFO;

enum
{
	HELP_ID_CHOOSING_MODULE,
	HELP_ID_PLACING_PICKUPS,
	HELP_ID_ADJUSTING_TRACK,
	HELP_ID_SAVING_TRACK,
	HELP_ID_LOADING_TRACK,
	HELP_ID_PLACING_MODULE,
	HELP_ID_MENU,

	HELP_ID_QUERY,

	HELP_ID_MAX_INDEX
};

void ShowHelp(void);
void HideHelp(void);
bool HelpExists(void);
const HELP_DIALOG_INFO* CurrentHelp(void);

#endif //_EDITORHELP_H