#include <math.h>
#include <string>
#include <assert.h>

#include "platform.h"
#include "render.h"
#include "editor.h"
#include "textstrings.h"

#include <mss.h>

#define _SOUNDS_ON
///#define DISPLAY_WALLS	// remove commment marks from start of make this enable display of boundary walls 
						// within the editor - NOT normally desired

//**********************************************
//
//**********************************************
using namespace std;
extern string				RTUPathName;

extern HINSTANCE			g_hInst;
extern HWND					g_hWnd;
extern LPDIRECT3DDEVICE3	D3DDevice;

extern int					g_DefaultScreenWidth;
extern int					g_DefaultScreenHeight;

extern CD3DFramework*		g_pFramework;

extern CURSORDESC			ModuleCursor;
extern CURSORDESC			PickupCursor;

extern DXUNIT**				DXUnits;

extern REAL					CursorFlashTime;

extern REAL					PickupSpin;

extern FONTDESCRIPTION		SprueTextFont;

static LPDIRECT3DTEXTURE2	TPageCache[TEX_COUNT];
static HAUDIO				SoundCache[SND_COUNT];

static TPAGEID				CurrentTexture = TEX_NOT_ASSIGNED;

static TCHAR				ButtonTextureName[] = "buttons.bmp";
static TCHAR				SprueWire1TextureName[] = "spruewire1.bmp";
static TCHAR				SprueWire2TextureName[] = "spruewire2.bmp";
static TCHAR				SprueFontTextureName[] = "spruefont.bmp";
static TCHAR				ClockTextureName[] = "clock.bmp";
static TCHAR				IconTextureName[] = "icons.bmp";

static D3DTLVERTEX			SpriteVerts[4];
static DWORD				SpriteClipOption = D3DDP_DONOTCLIP;

static TPAGEID NextTPage[] = {
		TEX_UNITS_01,
		TEX_UNITS_02,
		TEX_UNITS_03,
		TEX_UNITS_04,
		TEX_UNITS_05,
		TEX_UNITS_06,
		TEX_UNITS_07,
		TEX_UNITS_08,
		TEX_UNITS_09,
		TEX_UNITS_10,
		TEX_UNITS_11
};

static U16					TextureChanges;
static TPAGEID				CurrentTexturePage;

#ifdef NEW_WALLS
REVOLTMATRIX				WallMatrix[6];
#else
REVOLTMATRIX				WallMatrix[5];
#endif

static D3DVERTEX	UnitRootVerts[16];
static WORD			UnitRootIndices[24] = { 8, 1, 0, 8, 9, 1,
										10, 3, 2, 10, 11, 3,
										12, 5, 4, 12, 13, 5,
										14, 7, 6, 14, 15, 7
									 };

static D3DLVERTEX	BoxVerts[8];
static WORD			BoxLines[24] = { 0, 1, 1, 2, 2, 3, 3, 0,
									 4, 5, 5, 6, 6, 7, 7, 4,
									 0, 4, 1, 5, 2, 6, 3, 7
								   };

static const REAL	BoxColor[3] = {Real(0.7f), Real(0.2f), ZERO};

//**********************************************
//
//**********************************************
static LPDIRECTINPUT		DirectInputObject	= NULL;
static LPDIRECTINPUTDEVICE	KeyboardDevice		= NULL;
static BUTTON_STATES		PreviousButtons		= 0L;
static BUTTON_STATES		CurrentButtons		= 0L;

typedef struct
{
	U32	ID;
	BUTTON_STATES Flags;
}BUTTON_PAIRS;

static BUTTON_PAIRS ButtonTable[] =
{
	{DIK_UP,BUTTON_UP},
	{DIK_DOWN, BUTTON_DOWN},
	{DIK_LEFT, BUTTON_LEFT},
	{DIK_RIGHT, BUTTON_RIGHT},
	{DIK_RETURN, BUTTON_CONFIRM},
	{DIK_ESCAPE, BUTTON_CANCEL},
	{DIK_PRIOR, BUTTON_PRIOR},
	{DIK_NEXT, BUTTON_NEXT},
	{DIK_ADD, BUTTON_ROT_LEFT},
	{DIK_SUBTRACT, BUTTON_ROT_RIGHT},
	{DIK_DELETE, BUTTON_DELETE},
	{DIK_INSERT, BUTTON_INSERT},
	{DIK_LSHIFT, BUTTON_SHIFT},
	{DIK_RSHIFT, BUTTON_SHIFT},
	{DIK_P, BUTTON_PICKUP},
	{DIK_C, BUTTON_PICKUP},
	{DIK_X, BUTTON_NEXT_VARIANT},
	{DIK_Z, BUTTON_PREVIOUS_VARIANT},
	{DIK_SPACE, BUTTON_ROTATE_MODULE},
	{DIK_BACK, BUTTON_ERASE},
	{DIK_T, BUTTON_TEXTURE_TOGGLE},
	{DIK_RETURN, BUTTON_PLACE_MODULE},
};

//**********************************************
//
//**********************************************
void LoadSound(SND_TYPE sound, char* filename);

//**********************************************
//
//**********************************************
void InitializeTextureCache(void)
{
	char filename[MAX_PATH];
	
	TPageCache[TEX_UNITS_00] = NULL;
	for(U16 i = TEX_UNITS_01; i < TEX_UNITS_COUNT; i++)
	{
		sprintf(filename, "%s\\tpage_%02d.bmp", RTUPathName.c_str(), i);
		TPageCache[i] = D3DTextr_GetTexture(filename);
	}

	DDCOLORKEY ck;

	ck.dwColorSpaceLowValue = 0;
	ck.dwColorSpaceHighValue = 0;

	LPDIRECTDRAWSURFACE4 surf;

	TPageCache[TEX_SPRUE_BUTTONS] = D3DTextr_GetTexture(ButtonTextureName);
	surf = D3DTextr_GetSurface(ButtonTextureName);
	surf->SetColorKey(DDCKEY_SRCBLT, &ck);

	TPageCache[TEX_SPRUE_WIRE1] = D3DTextr_GetTexture(SprueWire1TextureName);
	surf = D3DTextr_GetSurface(SprueWire1TextureName);
	surf->SetColorKey(DDCKEY_SRCBLT, &ck);
	
	TPageCache[TEX_SPRUE_WIRE2] = D3DTextr_GetTexture(SprueWire2TextureName);
	surf = D3DTextr_GetSurface(SprueWire2TextureName);
	surf->SetColorKey(DDCKEY_SRCBLT, &ck);
	
	TPageCache[TEX_SPRUE_FONT] = D3DTextr_GetTexture(SprueFontTextureName);
	surf = D3DTextr_GetSurface(SprueFontTextureName);
	surf->SetColorKey(DDCKEY_SRCBLT, &ck);
	
	TPageCache[TEX_ALARM_CLOCK] = D3DTextr_GetTexture(ClockTextureName);
	surf = D3DTextr_GetSurface(ClockTextureName);
	surf->SetColorKey(DDCKEY_SRCBLT, &ck);

	TPageCache[TEX_ICONS] = D3DTextr_GetTexture(IconTextureName);
	surf = D3DTextr_GetSurface(IconTextureName);
	surf->SetColorKey(DDCKEY_SRCBLT, &ck);
	
}

void LoadBitmaps(void)
{
	char filename[MAX_PATH];
	
	for(U16 i = TEX_UNITS_00; i < TEX_UNITS_COUNT; i++)
	{
		sprintf(filename, "%s\\tpage_%02d.bmp", RTUPathName.c_str(), i);
		D3DTextr_CreateTexture(filename);
	}
	D3DTextr_CreateTexture(ButtonTextureName);
	D3DTextr_CreateTexture(SprueWire1TextureName);
	D3DTextr_CreateTexture(SprueWire2TextureName);
	D3DTextr_CreateTexture(SprueFontTextureName);
	D3DTextr_CreateTexture(ClockTextureName);
	D3DTextr_CreateTexture(IconTextureName);
}

void SetCurrentTexture(TPAGEID	newtexture)
{
	if(newtexture != CurrentTexture)
	{
		if(newtexture != TEX_NOT_ASSIGNED)
		{
			D3DDevice->SetTexture( 0, TPageCache[newtexture]);
		}
		else
		{
			D3DDevice->SetTexture( 0, NULL);
		}
		CurrentTexture = newtexture;
	}
}

void InitializeSoundSystem(void)
{
#ifdef _SOUNDS_ON
	AIL_quick_startup(1, 0, 22050, 16, 2);
	LoadSound(SND_QUERY, "sounds\\choice.wav");
	LoadSound(SND_MENU_OUT, "sounds\\menuout.wav");
	LoadSound(SND_MENU_IN, "sounds\\menuin.wav");
	LoadSound(SND_MENU_MOVE, "sounds\\menumove.wav");
	LoadSound(SND_CONGRATS, "sounds\\exptcomp.wav");
	LoadSound(SND_WARNING, "sounds\\warning.wav");
	LoadSound(SND_TICK_TOCK, "sounds\\ticktock.wav");
	LoadSound(SND_PLACE_MODULE, "sounds\\placemod.wav");
	LoadSound(SND_DELETE,  "sounds\\delete.wav");
	LoadSound(SND_ADJUST, "sounds\\enlrgred.wav");
	LoadSound(SND_LOWER, "sounds\\lower.wav");
	LoadSound(SND_RAISE, "sounds\\raise.wav");
	LoadSound(SND_PLACE_PICKUP, "sounds\\placepup.wav");
	LoadSound(SND_ROTATE, "sounds\\rotate.wav");
	LoadSound(SND_TYPING, "sounds\\typetext.wav");
#endif
}

void LoadSound(SND_TYPE sound, char* filename)
{
#ifdef _SOUNDS_ON
	assert(sound < SND_COUNT);			//Sound number is out of range

	SoundCache[sound] = AIL_quick_load(filename);	//attempt to load the sound

#ifdef _DEBUG
	if(SoundCache[sound] == NULL)
	{
		char* message = AIL_last_error();
		assert(FALSE);	//'message' holds the cause of the failure to load the specified sound
	}
#endif
#endif
}

void StartSound(SND_TYPE sound)
{
#ifdef _SOUNDS_ON
	assert(sound < SND_COUNT);			//Sound number is out of range
	assert(SoundCache[sound] != NULL);	//Sound is not present in cache

	AIL_quick_play(SoundCache[sound], 1);
#endif
}

void StartLoopingSound(SND_TYPE sound)
{
#ifdef _SOUNDS_ON
	assert(sound < SND_COUNT);			//Sound number is out of range
	assert(SoundCache[sound] != NULL);	//Sound is not present in cache

	AIL_quick_play(SoundCache[sound], 0);
#endif
}

void StopSound(SND_TYPE sound)
{
#ifdef _SOUNDS_ON
	assert(sound < SND_COUNT);			//Sound number is out of range
	assert(SoundCache[sound] != NULL);	//Sound is not present in cache

	AIL_quick_halt(SoundCache[sound]);
#endif
}

void ShutdownSoundSystem(void)
{
#ifdef _SOUNDS_ON
	AIL_quick_shutdown();
#endif
}

BOOL InitializeInputDevices(void)
{
    PreviousButtons = 0L;
	CurrentButtons = 0L;
	
	HRESULT hr; 
 
    // Create the DirectInput object. 
    hr = DirectInputCreate(g_hInst, DIRECTINPUT_VERSION, 
                           &DirectInputObject, NULL); 
    if FAILED(hr) return FALSE; 
 
    // Retrieve a pointer to an IDirectInputDevice interface 
    hr = DirectInputObject->CreateDevice(GUID_SysKeyboard, &KeyboardDevice, NULL); 
    if FAILED(hr) 
    { 
        ShutdownInputDevices(); 
        return FALSE; 
    } 
 
	// Now that you have an IDirectInputDevice interface, get 
	// it ready to use. 
 
    // Set the data format using the predefined keyboard data 
    // format provided by the DirectInput object for keyboards. 
    hr = KeyboardDevice->SetDataFormat(&c_dfDIKeyboard); 
    if FAILED(hr) 
    { 
        ShutdownInputDevices(); 
        return FALSE; 
    } 
 
    // Set the cooperative level 
    hr = KeyboardDevice->SetCooperativeLevel(g_hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE); 
    if FAILED(hr) 
    { 
        ShutdownInputDevices(); 
        return FALSE; 
    } 
 
    // Get access to the input device. 
    hr = KeyboardDevice->Acquire(); 
    if FAILED(hr) 
    { 
        ShutdownInputDevices(); 
        return FALSE; 
    } 
 
    return TRUE; 
} 
 
void ShutdownInputDevices(void)
{ 
    if (DirectInputObject) 
    { 
        if (KeyboardDevice) 
        { 
            // Always unacquire the device before calling Release(). 
             KeyboardDevice->Unacquire(); 
             KeyboardDevice->Release();
             KeyboardDevice = NULL; 
        } 
        DirectInputObject->Release();
        DirectInputObject = NULL; 
    } 
} 
 
 
void AcquireInputStatus(void) 
{ 
    char     buffer[256]; 
    HRESULT  hr; 
 
    hr = KeyboardDevice->GetDeviceState(sizeof(buffer),(LPVOID)&buffer); 
    if FAILED(hr) 
    { 
        // If it failed, the device has probably been lost. 
        // We should check for (hr == DIERR_INPUTLOST) 
        // and attempt to reacquire it here. 
		hr = KeyboardDevice->Acquire(); 
		if FAILED(hr)
		{
			return; 
		}
    } 

	PreviousButtons = CurrentButtons;
	CurrentButtons = 0;
	
	U16 buttonindex = sizeof(ButtonTable) / sizeof(BUTTON_PAIRS);
	while(buttonindex--)
	{
		if(buffer[ButtonTable[buttonindex].ID] & 0x80)
		{
			CurrentButtons |= ButtonTable[buttonindex].Flags;
		}
	}
} 

BOOL ButtonIsDown(BUTTON_STATES buttonmask)
{
	if((CurrentButtons & buttonmask) == buttonmask)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL ButtonJustWentDown(BUTTON_STATES buttonmask)
{
	if((CurrentButtons & buttonmask) == buttonmask)
	{
		if((PreviousButtons & buttonmask) != buttonmask)
		{
			return TRUE;
		}
	}
	return FALSE;
}

void SpriteClippingOff(void)
{
	SpriteClipOption = D3DDP_DONOTCLIP;
}

void SpriteClippingOn(void)
{
	SpriteClipOption = 0;
}

void InitializeSpritePoly(void)
{
	SpriteVerts[3] = D3DTLVERTEX(D3DVECTOR(400, 32, 0.1f), 0.5f,0xffffffffL, 0xff000000l, ZERO, 0.125f);
	SpriteVerts[1] = D3DTLVERTEX(D3DVECTOR(400, 0, 0.1f), 0.5f, 0xffffffffL, 0xff000000l, ZERO, ZERO);
	SpriteVerts[2] = D3DTLVERTEX(D3DVECTOR(432, 32, 0.1f), 0.5f, 0xffffffffL, 0xff000000l, 0.125f, 0.125f);
	SpriteVerts[0] = D3DTLVERTEX(D3DVECTOR(432, 0, 0.1f), 0.5f, 0xffffffffL, 0xff000000l, 0.125f, ZERO);
}

void SetSpriteColor(U32 color)
{
	D3DCOLOR newcolor = color | 0xff000000l;
	SpriteVerts[0].color = newcolor;
	SpriteVerts[1].color = newcolor;
	SpriteVerts[2].color = newcolor;
	SpriteVerts[3].color = newcolor;
}

void DrawSprite(REAL left, REAL top, REAL right, REAL bottom, REAL u0, REAL v0, REAL u1, REAL v1)
{
    // Get dimensions of the render surface 
    DDSURFACEDESC2 ddsd;
    ddsd.dwSize = sizeof(DDSURFACEDESC2);
    g_pFramework->GetRenderSurface()->GetSurfaceDesc(&ddsd);

    REAL xscale = ((REAL)g_DefaultScreenWidth) / ddsd.dwWidth; 
    REAL yscale = ((REAL)g_DefaultScreenHeight) / ddsd.dwHeight; 

	left /= xscale;
	right /= xscale;
	top /= yscale;
	bottom /= yscale;

	HRESULT hResult;

	SpriteVerts[1].sx = left;
	SpriteVerts[1].sy = top;
	SpriteVerts[1].tu = u0;
	SpriteVerts[1].tv = v0;
	
	SpriteVerts[3].sx = left;
	SpriteVerts[3].sy = bottom;
	SpriteVerts[3].tu = u0;
	SpriteVerts[3].tv = v1;
	
	SpriteVerts[0].sx = right;
	SpriteVerts[0].sy = top;
	SpriteVerts[0].tu = u1;
	SpriteVerts[0].tv = v0;
	
	SpriteVerts[2].sx = right;
	SpriteVerts[2].sy = bottom;
	SpriteVerts[2].tu = u1;
	SpriteVerts[2].tv = v1;
	hResult = D3DDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_TLVERTEX, (LPVOID)SpriteVerts, 4,  D3DDP_DONOTUPDATEEXTENTS | SpriteClipOption);
}

void DrawSpinningModule(const TRACKTHEME* theme)
{
	HRESULT hResult;

	DWORD fvf = D3DFVF_NORMAL | D3DFVF_XYZ | D3DFVF_TEX1;

	REVOLTMATRIX			camera;
	static REVOLTVECTOR	up = {ZERO, ONE, ZERO};
	static REVOLTVECTOR	from = {ZERO, -CameraHeight, -CameraHeight};
	static REVOLTVECTOR	at = {ZERO, ZERO, ZERO};

	MAKE_VIEW_MATRIX(camera, from, at, up);
	SET_VIEW_MATRIX(camera);

	REVOLTMATRIX ObjectMatrix;
	REVOLTMATRIX TransMatrix;
	REVOLTMATRIX ScaleMatrix;
	REVOLTMATRIX RotMatrix;
	static REAL spin = Real(0.0f);

	CurrentTexturePage = TEX_UNITS_00;
	
	while(CurrentTexturePage < theme->TPageCount)
	{
		SetCurrentTexture(CurrentTexturePage);
		TextureChanges++;

		INDEX m = theme->Lookup->Groups[ModuleCursor.Y].ModuleID;
		INDEX alt = theme->Lookup->Groups[ModuleCursor.Y].AlternateID;
		if((alt != m) && (ModuleCursor.X > 0))
		{
			m = alt;
		}
		
		TRACKMODULE* module = theme->Modules[m];
		REAL scale = (ONE / max(ModuleWidth(module), ModuleHeight(module))) * Real(2.0f);
		REAL xshift = (SMALL_CUBE_SIZE * (ModuleWidth(module) - 1)) / Real(2.0f);
		REAL yshift = (SMALL_CUBE_SIZE * (ModuleHeight(module) - 1)) / Real(2.0f);
		for(U32 i = 0; i < module->InstanceCount; i++)
		{
			RevoltTrackUnitInstance* instance = module->Instances[i];

			MAKE_SCALE_MATRIX(ScaleMatrix, scale, scale, scale);
			MAKE_TRANSLATE_MATRIX(TransMatrix, (instance->XPos * SMALL_CUBE_SIZE) - xshift, 0, (instance->YPos * SMALL_CUBE_SIZE) + yshift);
			MATRIX_MULTIPLY(ObjectMatrix, ScaleMatrix, TransMatrix);
			MAKE_ROTATE_Y_MATRIX(RotMatrix, spin);
			MATRIX_MULTIPLY(ObjectMatrix, RotMatrix, ObjectMatrix);
			MAKE_TRANSLATE_MATRIX(TransMatrix, -SMALL_CUBE_SIZE * 1.5, SMALL_CUBE_SIZE, 0);
			MATRIX_MULTIPLY(ObjectMatrix, TransMatrix, ObjectMatrix);
			
			SET_WORLD_MATRIX(ObjectMatrix);

			COMPONENT* pancomponent = DXUnits[instance->UnitID]->Components[PAN_INDEX];	//get pan component
			PRIMITIVE* panprim = pancomponent->Primitives[CurrentTexturePage];

			COMPONENT* pegcomponent = DXUnits[instance->UnitID]->Components[PEG_INDEX];	//get peg component
			PRIMITIVE* pegprim = pegcomponent->Primitives[CurrentTexturePage];

			if( ((panprim->VertexCount != 0) && (panprim->Vertices != NULL)) || ((pegprim->VertexCount != 0) && (pegprim->Vertices != NULL)) )
			{
				if((pegprim->VertexCount != 0) && (pegprim->Vertices != NULL) )
				{
					hResult = D3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, fvf, (LPVOID)pegprim->Vertices, pegprim->VertexCount,NULL);
				}
				if((panprim->VertexCount != 0) && (panprim->Vertices != NULL) )
				{
					hResult = D3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, fvf, (LPVOID)panprim->Vertices, panprim->VertexCount,NULL);
				}
			}
		}
		CurrentTexturePage = NextTPage[CurrentTexturePage];
	}
	spin += 0.01f;
}

void DrawTrack(const TRACKTHEME* theme, const TRACKDESC* track)
{
	DWORD fvf = D3DFVF_NORMAL | D3DFVF_XYZ | D3DFVF_TEX1;

	REVOLTMATRIX objectmatrix;

	CalculateWallMatrices(track);
	TextureChanges = 0;
	CurrentTexturePage = TEX_UNITS_00;
	
	while(CurrentTexturePage < theme->TPageCount)
	{
		SetCurrentTexture(CurrentTexturePage);
		TextureChanges++;
		for(S32 y = 0; y < track->Height; y++)
		{
			for(S32 x = 0; x < track->Width; x++)
			{
				U32 g = (y * track->Width) + x;
				U32 m = track->Units[g].UnitID;

				if(m != UNIT_SPACER)
				{
					COMPONENT* pancomponent = DXUnits[m]->Components[PAN_INDEX];	//get pan component
					PRIMITIVE* panprim = pancomponent->Primitives[CurrentTexturePage];

					COMPONENT* pegcomponent = DXUnits[m]->Components[PEG_INDEX];	//get peg component
					PRIMITIVE* pegprim = pegcomponent->Primitives[CurrentTexturePage];
					if( ((panprim->VertexCount != 0) && (panprim->Vertices != NULL)) || ((pegprim->VertexCount != 0) && (pegprim->Vertices != NULL)) )
					{
						SET_WORLD_MATRIX(track->Units[g].Matrix);
						if((pegprim->VertexCount != 0) && (pegprim->Vertices != NULL) )
						{
							REAL h = track->Units[g].Elevation * -ElevationStep;
							if(h != ZERO)
							{
								DrawUnitRoot(h);
							}
							D3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, fvf, (LPVOID)pegprim->Vertices, pegprim->VertexCount,NULL);
						}
						if((panprim->VertexCount != 0) && (panprim->Vertices != NULL) )
						{
							D3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, fvf, (LPVOID)panprim->Vertices, panprim->VertexCount,NULL);
						}
					}
				}
			}
		}
#ifdef DISPLAY_WALLS
		U16 w = theme->UnitCount - theme->WallIndex;
		while(w--)
		{
			COMPONENT* pancomponent = DXUnits[w + theme->WallIndex]->Components[PAN_INDEX];	//get pan component
			PRIMITIVE* panprim = pancomponent->Primitives[CurrentTexturePage];
			if( (panprim->VertexCount != 0) && (panprim->Vertices != NULL) )
			{
				SET_WORLD_MATRIX(WallMatrix[w]);
				D3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, fvf, (LPVOID)panprim->Vertices, panprim->VertexCount,NULL);
			}
		}
#endif
		CurrentTexturePage = NextTPage[CurrentTexturePage];
	}

	REVOLTMATRIX IdentityMatrix;
	MAKE_IDENTITY_MATRIX(IdentityMatrix);
	SET_WORLD_MATRIX(IdentityMatrix);
	SetCurrentTexture(TEX_NOT_ASSIGNED);

	static D3DLVERTEX		gridverts[2];
	static WORD				gridlines[2] = { 0, 1};

	gridverts[0].x = gridverts[1].x = -(SMALL_CUBE_SIZE / 2);
	gridverts[0].z = -(SMALL_CUBE_SIZE / 2);
	gridverts[1].z = gridverts[0].z + (track->Height * SMALL_CUBE_SIZE);

	gridverts[0].y = gridverts[1].y = 0;

	gridverts[0].color = gridverts[1].color = PURE_WHITE;
	gridverts[0].specular = gridverts[1].specular = PURE_WHITE;

	for(U16 n = 0; n <= track->Width; n++)
	{
		D3DDevice->DrawIndexedPrimitive(D3DPT_LINELIST, D3DFVF_LVERTEX, (LPVOID)gridverts, 2, gridlines, 2, NULL);
		gridverts[0].x = gridverts[1].x = (gridverts[0].x + SMALL_CUBE_SIZE);
	}

	gridverts[0].z = gridverts[1].z = -(SMALL_CUBE_SIZE / 2);
	gridverts[0].x = -(SMALL_CUBE_SIZE / 2);
	gridverts[1].x = gridverts[0].x + (track->Width * SMALL_CUBE_SIZE);

	for(n = 0; n <= track->Height; n++)
	{
		D3DDevice->DrawIndexedPrimitive(D3DPT_LINELIST, D3DFVF_LVERTEX, (LPVOID)gridverts, 2, gridlines, 2, NULL);
		gridverts[0].z = gridverts[1].z = (gridverts[0].z + SMALL_CUBE_SIZE);
	}

}

void InitializeCursorPrimitive(void)
{
	D3DCOLOR	diffuse, specular;

	diffuse = D3DRGB(BoxColor[0], BoxColor[1], BoxColor[2]);
	specular = D3DRGB(ZERO, ZERO, ZERO);
	REAL boxradius = SMALL_CUBE_SIZE / Real(1.6f);

	BoxVerts[0] =  D3DLVERTEX(D3DVECTOR(-boxradius, boxradius, boxradius), diffuse, specular, ZERO, ZERO);
	BoxVerts[1] =  D3DLVERTEX(D3DVECTOR(boxradius, boxradius, boxradius), diffuse, specular, ZERO, ZERO);
	BoxVerts[2] =  D3DLVERTEX(D3DVECTOR(boxradius, boxradius, -boxradius), diffuse, specular, ZERO, ZERO);
	BoxVerts[3] =  D3DLVERTEX(D3DVECTOR(-boxradius, boxradius, -boxradius), diffuse, specular, ZERO, ZERO);
	BoxVerts[4] =  D3DLVERTEX(D3DVECTOR(-boxradius, -boxradius, boxradius), diffuse, specular, ZERO, ZERO);
	BoxVerts[5] =  D3DLVERTEX(D3DVECTOR(boxradius, -boxradius, boxradius), diffuse, specular, ZERO, ZERO);
	BoxVerts[6] =  D3DLVERTEX(D3DVECTOR(boxradius, -boxradius, -boxradius), diffuse, specular, ZERO, ZERO);
	BoxVerts[7] =  D3DLVERTEX(D3DVECTOR(-boxradius, -boxradius, -boxradius), diffuse, specular, ZERO, ZERO);
}

void DrawCursorBox(void)
{
	REAL color[3] = {BoxColor[0], BoxColor[1], BoxColor[2]};
	if(CursorFlashTime > 0)
	{
		color[2] = ONE;
	}
		
	D3DCOLOR	diffuse;

	diffuse = D3DRGB(color[0], color[1], color[2]);

	for(U16 n = 0; n < 8; n++)
	{
		BoxVerts[n].color =  diffuse;
	}

	D3DDevice->DrawIndexedPrimitive(D3DPT_LINELIST, D3DFVF_LVERTEX, (LPVOID)BoxVerts, 8, BoxLines, 24, NULL);
}

void DrawPickupScreen(const TRACKTHEME* theme, const TRACKDESC* track)
{
	DrawPlanViewOfTrack(theme);
	static D3DLVERTEX		crosshairverts[4];
	static WORD				crosshairlines[4] = { 0, 1, 2, 3};

	SetCurrentTexture(TEX_NOT_ASSIGNED);
	REVOLTMATRIX IdentityMatrix;
	MAKE_IDENTITY_MATRIX(IdentityMatrix);						   
	D3DDevice->SetRenderState(D3DRENDERSTATE_TEXTUREHANDLE, 0);
	D3DDevice->SetRenderState(D3DRENDERSTATE_ZENABLE, FALSE);
	SET_WORLD_MATRIX(IdentityMatrix);

	//plot the main crosshairs
	crosshairverts[0].x = crosshairverts[1].x = PickupCursor.X * SMALL_CUBE_SIZE;
	crosshairverts[0].z = track->Height * SMALL_CUBE_SIZE;
	crosshairverts[1].z = -SMALL_CUBE_SIZE;
	crosshairverts[2].z = crosshairverts[3].z = PickupCursor.Y * SMALL_CUBE_SIZE;
	crosshairverts[2].x = track->Width* SMALL_CUBE_SIZE;
	crosshairverts[3].x = -SMALL_CUBE_SIZE;
	crosshairverts[0].y = crosshairverts[1].y = crosshairverts[2].y = crosshairverts[3].y = -ElevationStep;

	crosshairverts[0].color = crosshairverts[1].color = crosshairverts[2].color = crosshairverts[3].color = PURE_WHITE;
	crosshairverts[0].specular = crosshairverts[1].specular = crosshairverts[2].specular = crosshairverts[3].specular = PURE_WHITE;

	D3DDevice->DrawIndexedPrimitive(D3DPT_LINELIST, D3DFVF_LVERTEX, (LPVOID)crosshairverts, 4, crosshairlines, 4, NULL);
	
	static D3DLVERTEX	pickupverts[4];
	SetCurrentTexture(TEX_SPRUE_BUTTONS);

	pickupverts[0].x = pickupverts[2].x = - (SMALL_CUBE_SIZE / 2);
	pickupverts[0].z = pickupverts[1].z = - (SMALL_CUBE_SIZE / 2);
	pickupverts[1].x = pickupverts[3].x = SMALL_CUBE_SIZE / 2;
	pickupverts[2].z = pickupverts[3].z = SMALL_CUBE_SIZE / 2;
	pickupverts[0].y = pickupverts[1].y = pickupverts[2].y = pickupverts[3].y = -ElevationStep;

	pickupverts[0].color = pickupverts[1].color = pickupverts[2].color = pickupverts[3].color = PURE_WHITE;
	pickupverts[0].specular = pickupverts[1].specular = pickupverts[2].specular = pickupverts[3].specular = PURE_WHITE;

	int spin = PickupSpin * 8;

	pickupverts[0].tu = pickupverts[2].tu = (spin % 8) * 0.0625f;
	pickupverts[1].tu = pickupverts[3].tu = pickupverts[0].tu + 0.0625f;
	pickupverts[2].tv = pickupverts[3].tv = 0.9375f;
	pickupverts[0].tv = pickupverts[1].tv = 1.0f;
	
	//now plot any pickups which have been positioned
	U16 n = track->PickupsUsed;
	while(n--)
	{
		REVOLTMATRIX TranslateMatrix;
		MAKE_TRANSLATE_MATRIX(TranslateMatrix, (track->Pickups[n].X * SMALL_CUBE_SIZE), 0, (track->Pickups[n].Y * SMALL_CUBE_SIZE));
		SET_WORLD_MATRIX(TranslateMatrix);

		D3DDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, D3DFVF_LVERTEX, (LPVOID)pickupverts, 4,  D3DDP_DONOTUPDATEEXTENTS | SpriteClipOption);
	}

	DrawFilledFrame(448, 352, 5, 3);

	SetCurrentFont(&SprueTextFont);
	
	char pickupinfo[40];
	sprintf(pickupinfo, GetTextString(TEXT_PICKUPS_PLACED), track->PickupsUsed);
	DrawText(480, 384, pickupinfo);

	D3DDevice->SetRenderState(D3DRENDERSTATE_ZENABLE, TRUE);
}

void DrawUnitRoot(REAL h)
{
	HRESULT hResult;
	DWORD	  fvf = D3DFVF_NORMAL | D3DFVF_XYZ | D3DFVF_TEX1;

	D3DVERTEX* vert = &UnitRootVerts[0];
	vert->y = -h;
	vert++;
	vert->y = -h;
	vert++;
	vert->y = -h;
	vert++;
	vert->y = -h;
	vert++;
	vert->y = -h;
	vert++;
	vert->y = -h;
	vert++;
	vert->y = -h;
	vert++;
	vert->y = -h;

	hResult = D3DDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, fvf, (LPVOID)UnitRootVerts, 16, UnitRootIndices, 24, NULL);
}

void InitializeUnitRootPrimitive(void)
{
	D3DCOLOR	diffuse, specular;

	diffuse = D3DRGB(ONE, ONE, ONE);
	specular = D3DRGB(ZERO, ZERO, ZERO);

	REAL unitradius = SMALL_CUBE_SIZE / Real(2.0f);

	UnitRootVerts[0] =  D3DVERTEX(D3DVECTOR(-unitradius, unitradius, unitradius), D3DVECTOR(ZERO, ZERO, -ONE), ZERO, ZERO);
	UnitRootVerts[1] =  D3DVERTEX(D3DVECTOR(unitradius, unitradius, unitradius), D3DVECTOR(ZERO, ZERO, -ONE), ZERO, ZERO);
	UnitRootVerts[2] =  D3DVERTEX(D3DVECTOR(unitradius, unitradius, unitradius), D3DVECTOR(-ONE, ZERO, ZERO), ZERO, ZERO);
	UnitRootVerts[3] =  D3DVERTEX(D3DVECTOR(unitradius, unitradius, -unitradius), D3DVECTOR(-ONE, ZERO, ZERO), ZERO, ZERO);
	UnitRootVerts[4] =  D3DVERTEX(D3DVECTOR(unitradius, unitradius, -unitradius), D3DVECTOR(ZERO, ZERO, ONE), ZERO, ZERO);
	UnitRootVerts[5] =  D3DVERTEX(D3DVECTOR(-unitradius, unitradius, -unitradius), D3DVECTOR(ZERO, ZERO, ONE), ZERO, ZERO);
	UnitRootVerts[6] =  D3DVERTEX(D3DVECTOR(-unitradius, unitradius, -unitradius), D3DVECTOR(ONE, ZERO, ZERO), ZERO, ZERO);
	UnitRootVerts[7] =  D3DVERTEX(D3DVECTOR(-unitradius, unitradius, unitradius), D3DVECTOR(ONE, ZERO, ZERO), ZERO, ZERO);
	UnitRootVerts[8] =  D3DVERTEX(D3DVECTOR(-unitradius, 0, unitradius), D3DVECTOR(ZERO, ZERO, -ONE), ZERO, ZERO);
	UnitRootVerts[9] =  D3DVERTEX(D3DVECTOR(unitradius, 0, unitradius), D3DVECTOR(ZERO, ZERO, -ONE), ZERO, ZERO);
	UnitRootVerts[10] =  D3DVERTEX(D3DVECTOR(unitradius, 0, unitradius), D3DVECTOR(-ONE, ZERO, ZERO), ZERO, ZERO);
	UnitRootVerts[11] =  D3DVERTEX(D3DVECTOR(unitradius, 0, -unitradius), D3DVECTOR(-ONE, ZERO, ZERO), ZERO, ZERO);
	UnitRootVerts[12] =  D3DVERTEX(D3DVECTOR(unitradius, 0, -unitradius), D3DVECTOR(ZERO, ZERO, ONE), ZERO, ZERO);
	UnitRootVerts[13] =  D3DVERTEX(D3DVECTOR(-unitradius, 0, -unitradius), D3DVECTOR(ZERO, ZERO, ONE), ZERO, ZERO);
	UnitRootVerts[14] =  D3DVERTEX(D3DVECTOR(-unitradius, 0, -unitradius), D3DVECTOR(ONE, ZERO, ZERO), ZERO, ZERO);
	UnitRootVerts[15] =  D3DVERTEX(D3DVECTOR(-unitradius, 0, unitradius), D3DVECTOR(ONE, ZERO, ZERO), ZERO, ZERO);
}

