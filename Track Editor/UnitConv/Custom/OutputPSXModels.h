#ifndef _OUTPUTPSXMODEL_H
#define _OUTPUTPSXMODEL_H

#include "MappedVector.h"
#include "Primitives.h"
#include "..\trackedit\pcincl.h"

#include <Fname.h>
#include <endstrm.h>

typedef struct {
	float CentreX, CentreY, CentreZ, Radius;
	float Xmin, Xmax, Ymin, Ymax, Zmin, Zmax;
	short PolyNum, VertNum;
} CUBE_HEADER_LOAD;

typedef struct {

	REAL	XMin, XMax;
	REAL	YMin, YMax;
	REAL	ZMin, ZMax;

} BBOX;

typedef struct {
	short	Type;
	short	Material;
	PLANE	Plane;
	VEC		Vertex[4];
	BBOX	BBox;
} PSXCOLLPOLY;

#define QUAD			(0x1)

class PSXModelOutput
{
	protected:
		RevoltTheme* LocalTheme;
		EndianOutputStream* Output;
		EndianOutputStream* CollOutput;
		void OutputFloatAsShort(EndianOutputStream* stream, REAL value);
		void OutputModule(const RevoltModule* module, EndianOutputStream* stream);
		void CalculateD3DNormal(REVOLTVECTOR* normal, const REVOLTVECTOR* p0, const REVOLTVECTOR* p1, const REVOLTVECTOR* p2);
		void NullifyCuboid(CUBE_HEADER_LOAD* cuboid);
		void AddVectorToCuboid(const VEC* vect,  CUBE_HEADER_LOAD* cuboid);
		void FillPlane(PSXCOLLPOLY* collpoly);
		void FillCollisionPoly(PSXCOLLPOLY* collpoly, const RevoltPolygon* poly);
	public:
		PSXModelOutput();
		virtual void OutputModels(const string& fileroot, RevoltTheme* theme, EndianStream::Endian whichend);
};

#endif
