#if !defined(__endstrm_h)
#define __endstrm_h

#include <Windows.h>
#include <fstream>
#include "bobtypes.h"

class EndianInputStream
{
	char	*buffer;
	U32		offset;
	U32		size;

public:
	EndianInputStream(const char* filename);
	~EndianInputStream();

	bool is_open(void);
	void open(const char* filename);
	void GetChunk(void* buf, U32 len);
	U32 FTell(void);
	U8 GetText(char* text, U8 maxtextlen);
	U8 GetPaddedText(char* text, U8 maxtextlen);

	U8 GetU8(void);
	U16 GetU16(void);
	U32 GetU32(void);
	S8 GetS8(void);
	S16 GetS16(void);
	S32 GetS32(void);
	float GetFloat(void);
	
	U32 GetSize(void);
	bool SkipBytes(U32 count);
	char* GetBuffer(void);
};


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

#define START_FILE_BUFFER_SIZE 65536

class EndianOutputStream
{
private:
	char fname[256];
	char* buf;
	int index;
	int bufferSize;

public:
	EndianOutputStream(const char* filename);
	~EndianOutputStream();
	void PutU8(U8 data);
	void PutU16(U16 data);
	void PutU32(U32 data);
	void PutS8(S8 data);
	void PutS16(S16 data);
	void PutS32(S32 data);
	void PutFloat(float data);
	void PutPaddedText(const char* text);
	bool WriteFile();
	
	bool is_open();
	//bool is_good();
	void write(void* buffer, int len);
	void fputs(const char* str);
	U32 tellp();

	bool CheckSize(int len);
};

#endif